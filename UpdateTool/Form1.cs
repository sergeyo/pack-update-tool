﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
namespace UpdateTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string curVersion;
        public string installedVersion;
        public BetterWebClient webClient;
        public string packZip;
        public string vFile;

        public string getCurrentVersion()
        {
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls11;
            // Add TLS 1.2
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            return new WebClient().DownloadString("https://techno.itman.website/version.php");
        }

        public string getInstalledVersion()
        {
            vFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft\\versions\\techno-1.12.2\\version.ver");
            string installed = "N/A";
            if (File.Exists(vFile))
            {
                installed = File.ReadAllText(vFile);
            }
                return installed;
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            state_lbl.Text = "Downloading: "+ progressBar.Value+" %";
            progressBar.Value = e.ProgressPercentage;
        }


        private async void Completed(object sender, AsyncCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            } else
            {
                string packDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft\\versions\\techno-1.12.2");
                if (Directory.Exists(packDir))
                {
                    Directory.Move(packDir, packDir+"~old");
                }
                state_lbl.Text = "Installing...";
                await Task.Run(() =>
                {
                    ZipFile.ExtractToDirectory(packZip, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft\\versions"));
                });
                if (Directory.Exists(packDir + "~old"))
                {
                    foreach (string path in new List<string>{ "resourcepacks","journeymap", "shaderpacks","saves", "screenshots" })
                    {
                        if(Directory.Exists(packDir + "~old\\"+ path)) {
                            if(Directory.Exists(packDir + "\\" + path))
                            {
                                Directory.Delete(packDir + "\\" + path,true);
                            }
                            Directory.Move(packDir + "~old\\"+ path, packDir + "\\"+ path);
                        }
                    }
                    foreach (string path in new List<string> { "options.txt", "optionsof.txt", "optionsshaders.txt", "servers.dat" })
                    {
                        if (File.Exists(packDir + "~old\\" + path))
                        {
                            if(File.Exists(packDir + "\\" + path))
                            {
                                File.Delete(packDir + "\\" + path);
                            }
                            File.Move(packDir + "~old\\" + path, packDir + "\\"+ path);
                        }
                    }
                    Directory.Delete(packDir + "~old", true);
                }
                File.Delete(packZip);
                File.WriteAllText(vFile, curVersion);
                installed_v.Text = curVersion;
                state_lbl.Text = "Done";

                MessageBox.Show("Installation Completed Successfully");
            }
            
        }

        public async void downloadPack()
        {
            string packFile = "techno-1.12.2-build-"+curVersion+".zip";
            webClient = new BetterWebClient();
            webClient.AutoRedirect = true;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls11;
            // Add TLS 1.2
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            packZip = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft\\versions\\"+packFile);
            webClient.DownloadFileAsync(new Uri("https://api.bitbucket.org/2.0/repositories/sergeyo/itmans-tech-1.12.2/downloads/" + packFile), packZip);
            state_lbl.Text = "Downloading";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            curVersion = getCurrentVersion();
            installedVersion = getInstalledVersion();
            current_v.Text = curVersion;
            installed_v.Text = installedVersion;
            timer1.Enabled = false;
            if(curVersion != installedVersion)
            {
                button1.Enabled = true;
            }
            state_lbl.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            downloadPack();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://techno.itman.website/");
        }
    }
}
